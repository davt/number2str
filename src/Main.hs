module Main where

import Text.Read (readMaybe)
import System.Environment (getArgs)

thousandTable = [(1000, "thousand"),
                 (1000^2, "million"), 
                 (1000^3, "billion"),
                 (1000^4, "trillion")]

digits = words "zero one two three four five six seven eight nine"
teens = words "ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen"
tens = words "ten twenty thirty forty fifty sixty seventy eighty ninety"


int2str:: Integer -> String
int2str n
    | n < 10 = digits !! (fromIntegral n)
    | n < 20 = teens !! (fromIntegral $ n - 10)
    | n < 100 && n `mod` 10 == 0 = tens !! (fromIntegral $ (n `div` 10) - 1)
    | n < 100 = (int2str $ 10 * (n `div` 10)) ++ " " ++ case n `mod` 10 of
                    0 -> ""
                    x -> (int2str x)
    | n < 1000 = (int2str $ n `div` 100) ++ " hundred" ++ case n `mod` 100 of
                    0 -> ""
                    x -> " and " ++ (int2str x)
    | otherwise = (int2str $ n `div` roundNumber) ++ " " ++ wordNumber ++ case n `mod` roundNumber of
                    0 -> ""
                    x -> " " ++ (int2str x)
        where (roundNumber, wordNumber) = head $ dropWhile (\(x, _) -> n > 1000 * x) thousandTable


main :: IO ()
main = do
    [nStr] <- getArgs
    let maybeN = readMaybe nStr
    case maybeN of
        Just n -> putStrLn $ int2str n
        Nothing -> putStrLn "Not a valid integer"
