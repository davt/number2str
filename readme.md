# Number2Str

Convert a number from a decimal form to a words description. Written in Haskell as a learning exercise.

# Usage

Build:

```
$ cabal build
Resolving dependencies...
Configuring number2str-0.1.0.0...
Building number2str-0.1.0.0...
Preprocessing executable 'number2str' for number2str-0.1.0.0...
[1 of 1] Compiling Main             ( src/Main.hs, dist/build/number2str/number2str-tmp/Main.o )
Linking dist/build/number2str/number2str ...
```

And then run:

```
$ dist/build/number2str/number2str 1234546
one million two hundred and thirty four thousand five hundred and forty six
```
